# -*- coding: utf-8 -*-

import os
from collections import defaultdict
from operator import itemgetter
from itertools import groupby

# pip install tabulate
from tabulate import tabulate


class DataFrame:
    def __init__(self, headers, rows):
        self._headers = headers
        self._rows = rows

    @property
    def headers(self):
        return self._headers

    @headers.setter
    def headers(self, new_headers):
        assert len(self._headers) == len(new_headers)
        pass

    @property
    def rows(self):
        return self._rows

    def group_by(self, by):
        idx_by = self.headers.index(by)
        groups = groupby(self.rows, lambda x: x[idx_by])
        return GroupedDataFrame(by, self.headers, groups)

    def merge(self, df, by):
        rows = {}
        idx_by = self.headers.index(by)
        for row in self.rows:
            rows[row[idx_by]] = row
        rows2 = {}
        idx_by = df.headers.index(by)
        for row in df.rows:
            rows2[row[idx_by]] = row
        new_headers = [by] + list((set(self.headers) | set(df.headers)) - {by})
        newRows = []
        for key in set(rows.keys()) | set(rows2.keys()):
            row = []
            for header in new_headers:
                if header in self.headers:
                    row.append(rows[key][self.headers.index(header)])
                else:
                    row.append(rows2[key][df.headers.index(header)])
            newRows.append(row)
        return DataFrame(new_headers, newRows)
        pass

    def __getitem__(self, name):
        res = []
        idx = self.headers.index(name)
        for row in self.rows:
            res.append(row[idx])
        return res

    def __setitem__(self, name, value):
        if name in self.headers:
            idx = self.headers.index(name)
            for i in range(len(value)):
                self.rows[i][idx] = value[i]
        else:
            self.headers.append(name)
            for i in range(len(value)):
                self.rows[i].append(value[i])

    def __str__(self):
        return tabulate(self.rows, headers=self.headers)

    @staticmethod
    def from_file(path):
        file = open(path)
        headers = file.readline().strip().split(',')
        rows = list(
            map(lambda x: list(map(lambda y: float(y) if y.replace('.', '', 1).isdigit() else y, x.strip().split(','))),
                file.readlines()))
        return DataFrame(headers, rows)

    def to_csv(self, path):
        file = open(path, 'w')
        print(",".join(self.headers), file=file)
        for row in self.rows:
            print(",".join(map(str, row)), file=file)
        file.close()
        return


class GroupedDataFrame:
    def __init__(self, by, headers, groups):
        self._by = by
        self._headers = headers
        self._groups = groups

    def sum_by(self, by):
        rows = []
        for key, group in self._groups:
            idx_by = self._headers.index(by)
            res = 0
            for row in group:
                res += list(row)[idx_by]
            rows.append([key, res])
        headers = [self._by, by]
        return DataFrame(headers, rows)


BASE_PATH = "."
PATH = os.path.join(BASE_PATH, 'spb_districts.csv')
POPULATION_PATH = os.path.join(BASE_PATH, 'spb_population_by_district.csv')
CAMERAS_PATH = os.path.join(BASE_PATH, 'cameras_per_district.csv')

# ===================================================
df = DataFrame.from_file(PATH)
amount_df = df.group_by('Район')
amount_df = amount_df.sum_by('Число камер')
amount_df.headers = ['Район', 'Число Камер']
amount_df.to_csv('cameras_per_district.csv')
print(amount_df)
# Район                Число Камер
# -----------------  -------------
# Адмиралтейский               396
# Василеостровский             588
# Выборгский                  3299
# Калининский                 3369
# Кировский                    732
# ...


# ===================================================
amount_df = DataFrame.from_file(CAMERAS_PATH)
pop_df = DataFrame.from_file(POPULATION_PATH)
full_df = amount_df.merge(pop_df, by='Район')
print(full_df)
# Район                Число Камер    Население    Площадь
# -----------------  -------------  -----------  ---------
# Калининский                 3369       538258      40.18
# Выборгский                  3299       509592     115.52
# Фрунзенский                 2787       401410      37.52
# Красногвардейский           2379       357906      56.35
# ...

n = full_df["Население"]
s = full_df["Площадь"]
full_df['Плотность'] = [float(n[i]) / float(s[i]) for i in range(len(n))]
full_df.to_csv('exam_done.csv')
print(full_df)
# Район                Число Камер    Население    Площадь    Плотность
# -----------------  -------------  -----------  ---------  -----------
# Калининский                 3369       538258      40.18      13396.2
# Выборгский                  3299       509592     115.52      4411.29
# ...
